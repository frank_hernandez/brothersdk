# BROTHERSDK

[![CI Status](https://img.shields.io/travis/Frank Hernandez/BROTHERSDK.svg?style=flat)](https://travis-ci.org/Frank Hernandez/BROTHERSDK)
[![Version](https://img.shields.io/cocoapods/v/BROTHERSDK.svg?style=flat)](https://cocoapods.org/pods/BROTHERSDK)
[![License](https://img.shields.io/cocoapods/l/BROTHERSDK.svg?style=flat)](https://cocoapods.org/pods/BROTHERSDK)
[![Platform](https://img.shields.io/cocoapods/p/BROTHERSDK.svg?style=flat)](https://cocoapods.org/pods/BROTHERSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BROTHERSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BROTHERSDK'
```

## Author

Frank Hernandez, hernandez.f@rouninlabs.com

## License

BROTHERSDK is available under the MIT license. See the LICENSE file for more info.
